#!/bin/bash
# 
# File:   include.sh
# @copyright (c) 2015, Alexander Shulzhenko,  contact@alexshulzhenko.ru
# @license GPL 3.0, http://opensource.org/licenses/GPL-3.0
#
# Created on Mar 7, 2015, 8:18:20 PM
#

#
# SYSTEM SETTINGS
# DON'T TOUCH UNLESS YOU ARE SURE
# START
configs='./settings/'



#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# system functions
# Example of usage
# is_identical '/home/sh/bash/configs/sh/set.sh' '/home/sh/bash/configs/sh/set.sh'
# returns whether files have the same content or not
is_identical ()
{
 if [ ! -e "$1" ];
 then
    msg $1 'source file not found'
    return 2
  fi

 # destination file doesn't exist so it will be created
  if [ ! -e "$2" ];
  then    
    return 3
  fi

  # first file
  first=($(md5sum "$1"))
  # second file
  second=($(md5sum "$2"))

  if [ "$first" = "$second" ];
  then
    return 0
  fi
  return 1
}


is_host_accessible ()
{
  ssh  -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no  -p $1  -o 'BatchMode yes' $2@$3 exit  
  res=$?
  if [ ! $res -eq  0 ];
  then
    echo "Can't get access to server, you need have access via ssh keys to use this utility"
    exit
  else
    echo "Successfully established connection"
  fi
}


_ERR_HDR_FMT="%.23s %s[%s]: "
_ERR_MSG_FMT="${_ERR_HDR_FMT}%s\n"

msg() {
  printf "$_ERR_MSG_FMT" $(date +%F.%T.%N) ${BASH_SOURCE[1]##*/} ${BASH_LINENO[0]} "${@}"
}


# END
#~~~~~~~~~~~~~~~~~~~~~~~
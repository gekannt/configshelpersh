#!/bin/bash
# 
# File:   config.sh
# @copyright (c) 2015, Alexander Shulzhenko,  contact@alexshulzhenko.ru
# @license GPL 3.0, http://opensource.org/licenses/GPL-3.0
#
# Created on Mar 6, 2015, 2:52:59 AM
#

DIR="${BASH_SOURCE%/*}"
if [[ ! -d "$DIR" ]];
 then DIR="$PWD";
fi
. "$DIR/include.sh"
. "$DIR/config.sh"

if [ -z $port  ];
then
    for i in ${sourcefiles[@]}; do
     path=${configs}`basename $i`;

     is_identical  "$path" "$i"
     res=$?

     if [ $res -eq 1 ] || [ $res -eq 3 ];
     then
       cp $path ${i}
       if [ $? -eq 0 ];
       then
        msg "file ${i}  is updated to the last version from ${path} "
       else
        msg "copying failed, perhaps you need run command as root"
       fi
     elif [ $res -eq 0 ];
     then
      msg "files ${path} and ${i} are the same, no copying"
     elif [ $res -eq 2 ];
     then
      msg "source ${path} file doesn't exits so nothing can be copied to ${i}"
     fi
    done
else
    is_host_accessible $port $remote_user $remote_host

    files_source=' '
    files_destination=' '
    for i in ${sourcefiles[@]}; do
     files_destination=" $remote_user@$remote_host:$i "
     files_source=" ${configs}`basename $i` "
     echo "passing ${files_source} to  ${files_destination}"
     rsync -L -e "ssh  -p  $port"   $files_source  $files_destination
    done
fi



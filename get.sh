#!/bin/bash
# 
# File:   get.sh
# @copyright (c) 2015, Alexander Shulzhenko,  contact@alexshulzhenko.ru
# @license GPL 3.0, http://opensource.org/licenses/GPL-3.0
#
# Created on Mar 6, 2015, 2:52:59 AM
#

DIR="${BASH_SOURCE%/*}"
if [[ ! -d "$DIR" ]];
 then DIR="$PWD";
fi
. "$DIR/include.sh"
. "$DIR/config.sh"



if [ -z $port  ];
then
    for i in ${sourcefiles[@]}; do
     path=${configs}`basename $i`;

     is_identical "$i" "$path"
     res=$?

     if [ $res -eq 1 ] || [ $res -eq 3 ];
     then
       cp ${i} $path
       msg 'file is copied to '${path}
     elif [ $res -eq 0 ];
     then
       msg "file is the same, skipping "${i}
     elif [ $res -eq 2 ];
     then
       msg "source file {$i}  not found"
     else
       msg "uknown error"${res}
     fi
    done

else
    is_host_accessible $port $remote_user $remote_host

    files=' '
    for i in ${sourcefiles[@]}; do
     files+=" $remote_user@$remote_host:$i "
    done

    rsync -L -v -e "ssh  -p  $port"    $files ./settings
fi
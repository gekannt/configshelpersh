#!/bin/bash
# 
# File:   config.sh
# @copyright (c) 2015, Alexander Shulzhenko,  contact@alexshulzhenko.ru
# @license GPL 3.0, http://opensource.org/licenses/GPL-3.0
#
# Created on Mar 7, 2015, 11:26:26 AM


#set this variable to anything else to work with local settings e.g.
# remote_server=''

remote_server='yes'



# CONFIGURATION TO WORK WITH LOCAL FILES
# following fields are not needed for editing local configs
port=''
remote_host=''
remote_user=''



if [ "$remote_server" == "yes"  ];
then
    # CONFIGURATION TO WORK WITH REMOTE SERVER
    port='22' # by default
    remote_host='remote_server_address.ru' # ip or name of remote server
    remote_user='root'  # or any other name, but please note that user  HAS TO
    #   have read/write access to configuration files if you want to read or
    # write respectively
fi


# EXAMPLE
# List files here which you would like to syncronise
sourcefiles=(
'/etc/php5/apache2/php.ini' 
'/etc/apache2/apache2.conf'
# and so on
);

